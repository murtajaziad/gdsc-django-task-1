from django.db import models

class Auth(models.Model):
  a_id = models.IntegerField(primary_key=True)
  name = models.CharField(max_length=100)

class Section(models.Model):
  ca_id = models.IntegerField(primary_key=True)
  name = models.CharField(max_length=100)
  is_active = models.BooleanField()

class Product(models.Model):
  p_id = models.IntegerField(primary_key=True)
  name = models.CharField(max_length=100)
  section = models.ForeignKey(Section, on_delete=models.CASCADE)
  price = models.FloatField()
  image = models.URLField()
  auth = models.ForeignKey(Auth, on_delete=models.CASCADE)
  is_active = models.BooleanField()
  is_rare = models.BooleanField()
  category = models.CharField(max_length=50, choices=models.TextChoices("scientific", "art", "historic", "novels", "fictional"))
  language = models.CharField(max_length=50, choices=models.TextChoices("arabic", "english", "french"))

class Address(models.Model):
  a_id = models.IntegerField(primary_key=True)
  address_type = models.CharField(max_length=50, choices=models.TextChoices("work", "home", "other"))
  address = models.CharField(max_length=100)

class Customer(models.Model):
  c_id = models.IntegerField(primary_key=True)
  name = models.CharField(max_length=100)
  email = models.CharField(max_length=100)
  add_id = models.ForeignKey(Address, on_delete=models.CASCADE)
  phone = models.CharField(max_length=100)
  gender = models.BooleanField()

class Item(models.Model):
  it_id = models.IntegerField(primary_key=True)
  p_id = models.ForeignKey(Product, on_delete=models.CASCADE)
  c_id = models.ForeignKey(Customer, on_delete=models.CASCADE)
  item_qty = models.IntegerField()

class Branch(models.Model):
  b_id = models.IntegerField(primary_key=True)
  branch_name = models.CharField(max_length=100)
  address = models.ForeignKey(Address, on_delete=models.CASCADE)
  phone = models.FloatField()

class Employee(models.Model):
  e_id = models.IntegerField(primary_key=True)
  name = models.CharField(max_length=100)
  position = models.CharField(max_length=100)
  salary = models.FloatField()
  is_admin = models.BooleanField()
  b_id = models.ForeignKey(Branch, on_delete=models.CASCADE)

class Order(models.Model):
  o_id = models.IntegerField(primary_key=True)
  items = models.ManyToManyField(Item)
  total = models.FloatField()
  price = models.FloatField()
  c_id = models.ForeignKey(Customer, on_delete=models.CASCADE)
  branch = models.ForeignKey(Branch, on_delete=models.CASCADE)
  e_id = models.ForeignKey(Employee, on_delete=models.CASCADE)
  delivery_type = models.CharField(max_length=50, choices=models.TextChoices("internal", "external"))